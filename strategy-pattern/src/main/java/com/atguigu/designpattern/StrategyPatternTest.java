package com.atguigu.designpattern;


import com.atguigu.designpattern.strategy.SortService;
import com.atguigu.designpattern.strategy.impl.BubbleSortStrategy;
import com.atguigu.designpattern.strategy.impl.QuickSortStrategy;

/**
 * @author lfy
 * @Description  模板模式定义大框架、策略默认定义小细节
 * @create 2022-12-28 21:17
 */
public class StrategyPatternTest {

    public static void main(String[] args) {
        Integer[] arr = new Integer[]{2,4,6,3,1,7,9,8};

        SortService sortService = new SortService(new BubbleSortStrategy());
        sortService.sort(arr);

        System.out.println("===============");

        //更新策略
        sortService.setStrategy(new QuickSortStrategy());
        sortService.sort(arr);

    }
}
