package com.atguigu.designpattern.strategy;

/**
 * @author lfy
 * @Description 算法接口：排序策略
 * @create 2022-12-29 20:36
 */
public interface SortStrategy {

    /**
     * 排序
     */
    void sort(Integer[] arr);
}
